﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTransfer.Models
{
    public class Transaction
    {
        [Key]
        public int ID { get; set; }

        public int Sum { get; set; }
        public string SenderId { get; set; }
        public User Sender { get; set; }
        public string RecipientId { get; set; }
        public User Recipient { get; set; }
        public DateTime TransactionDate { get; set; }
    }
}
