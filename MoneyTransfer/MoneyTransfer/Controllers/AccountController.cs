﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using MoneyTransfer.Models;
using MoneyTransfer.ViewModels;
using Microsoft.AspNetCore.Authorization;

namespace MoneyTransfer.Controllers
{
    public class AccountController : Controller
    {
        private ApplicationContext context;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private Checks checks;

        private Random rand;

        public AccountController(UserManager<User> userManager, SignInManager<User> signInManager, ApplicationContext context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            rand = new Random();
            this.context = context;
            this.checks = new Checks();
        }
        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                User user = new User { Email = model.Email, UserName = model.Email, PersonalAccount = GetPersonalAccount(), Balance = 1000 };
                // добавляем пользователя
                var result = await _userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    // установка куки
                    await _signInManager.SignInAsync(user, false);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                }
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult Login(string returnUrl = null)
        {
            return View(new LoginViewModel { ReturnUrl = returnUrl });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                string CurrentEmail = model.Email;
                bool IsItEmail = context.Users.Any(u => u.Email == model.Email);
                
                if (IsItEmail == false)
                {
                    User user = context.Users.FirstOrDefault(u => u.PersonalAccount == model.Email);
                    CurrentEmail = user.Email;
                }

                var result =
                    await _signInManager.PasswordSignInAsync(CurrentEmail, model.Password, model.RememberMe, false);
                if (result.Succeeded)
                {
                    // проверяем, принадлежит ли URL приложению
                    if (!string.IsNullOrEmpty(model.ReturnUrl) && Url.IsLocalUrl(model.ReturnUrl))
                    {
                        return Redirect(model.ReturnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Неправильный логин и (или) пароль");
                }
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LogOff()
        {
            // удаляем аутентификационные куки
            await _signInManager.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }

        private string GetPersonalAccount()
        {
            string personalAccount = checks.PersonalAccountGeneration(rand);

            if (!checks.CheckForUniqueness(personalAccount, context))
            {
                GetPersonalAccount();
            }

            return personalAccount;
        }

        [Authorize]
        public IActionResult AccountInfo()
        {
            User myUser = _userManager.Users.FirstOrDefault(r => r.Email == User.Identity.Name);
            return View(myUser);
        }

        [HttpGet]
        public IActionResult AddBalance()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddBalance(AddBalanceViewModel model)
        {
            string message = string.Empty;

            if (ModelState.IsValid)
            {
                User userTo = new User();
                User userFrom = new User();

                if (checks.CheckForUserExistence(model.PersonalAccount, ref userTo, context))
                    if (CurrentUserExist(ref userFrom))
                    {
                        if (checks.CheckForRequiredSum(model.Sum, userFrom))
                        {
                            if (userFrom != userTo)
                            {
                                MakePaymentFromUser(userFrom, userTo, model.Sum);
                            }
                            else
                            {
                                message = "Вы не можете сами себе перевести на счет";
                            }
                        }
                        else
                        {
                            message = "Не хватает средств на Вашем счету";
                        }
                    }
                    else
                    {
                        MakePaymentFromAnonym(userTo, model.Sum);
                    }
                else
                {
                    message = "Такого счета нет";
                }
            }
            if (message != string.Empty)
            {
                ModelState.AddModelError("", message);
                return View();
            }
            return RedirectToAction("Index", "Home"); ;
        }

        private bool CurrentUserExist(ref User userFrom)
        {
            if (User.Identity.IsAuthenticated)
            {
                userFrom = context.Users.FirstOrDefault(u => u.Email == User.Identity.Name);
                return true;
            }
            return false;
        }

        private void MakePaymentFromAnonym(User userTo, int sum)
        {
            userTo.Balance += sum;
            CreateTransaction(userTo, sum);
        }

        private void MakePaymentFromUser(User userFrom, User userTo, int sum)
        {
            userTo.Balance += sum;
            userFrom.Balance -= sum;
            CreateTransaction(userTo, sum);
        }

        private void CreateTransaction(User userTo, int sum)
        {
            Transaction transaction = new Transaction()
            {
                Sum = sum,
                RecipientId = userTo.Id,
                TransactionDate = DateTime.Now
            };

            User userFrom = new User();
            if (CurrentUserExist(ref userFrom))
            {
                transaction.SenderId = userFrom.Id;
            }

            context.Transactions.Add(transaction);
            context.SaveChanges();
        }

        [HttpGet]
        [Authorize]
        public IActionResult PaymentHistory(DateTime? dateFrom, DateTime? dateTo)
        {
            User user = context.Users.FirstOrDefault(u => u.Email == User.Identity.Name);
            IQueryable<Transaction> transactions = context.Transactions.OrderByDescending(t => t.TransactionDate).Where(t => t.SenderId == user.Id || t.RecipientId == user.Id);

            if (dateFrom != null)
            {
                transactions = transactions.Where(t => t.TransactionDate >= dateFrom.Value);
            }
            if (dateTo != null)
            {
                transactions = transactions.Where(t => t.TransactionDate <= dateTo.Value);
            }

            PaymentHistoryViewModel paymentHistoryViewModel = new PaymentHistoryViewModel
            {
                Users = context.Users.ToList<User>(),
                Transactions = transactions,
                FilterViewModel = new FilterViewModel(dateFrom, dateTo),
            };
            return View(paymentHistoryViewModel);
        }
    }

    public class Checks
    {
        public bool CheckForUniqueness(string personalAccount, ApplicationContext context)
        {
            bool IsUnique = !context.Users.Any(u => u.PersonalAccount == personalAccount);

            return IsUnique;
        }

        public bool CheckForUserExistence(string personalAccount, ref User user, ApplicationContext context)
        {
            bool exist = context.Users.Any(u => u.PersonalAccount == personalAccount);
            if (exist)
            {
                user = context.Users.FirstOrDefault(u => u.PersonalAccount == personalAccount);
            }

            return exist;
        }

        public bool CheckForRequiredSum(int sum, User userFrom)
        {
            bool enough = userFrom.Balance >= sum;

            return enough;
        }

        public string PersonalAccountGeneration(Random rand)
        {
            string personalAccount = string.Empty;
            for (int i = 0; i < 6; i++)
            {
                personalAccount = personalAccount + rand.Next(0, 10).ToString();
            }

            return personalAccount;
        }
    }
}
