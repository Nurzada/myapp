﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MoneyTransfer.Models;
using Microsoft.AspNetCore.Authorization;

namespace MoneyTransfer.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationContext context;

        public HomeController(ApplicationContext context)
        {
            this.context = context;
        }

        public IActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            { 
                ViewBag.Balance = GetBalance();
            }
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [Authorize]
        private int GetBalance()
        {
            int balance = 0;
            User user = context.Users.FirstOrDefault(u => u.Email == User.Identity.Name);
            if (user != null)
            {
                balance = user.Balance;
            }

            return balance;
        }
    }
}
