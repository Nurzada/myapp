﻿using MoneyTransfer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTransfer.ViewModels
{
    public class PaymentHistoryViewModel
    {
        public List<User> Users { get; set; }
        public IQueryable<Transaction> Transactions { get; set; }
        public FilterViewModel FilterViewModel { get; set; }
    }
}
