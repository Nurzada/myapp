﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTransfer.ViewModels
{
    public class FilterViewModel
    {
        public DateTime? SelectedDateFrom { get; private set; }
        public DateTime? SelectedDateTo { get; private set; }
       
        public FilterViewModel(DateTime? selectedDateFrom, DateTime? selectedDateTo)
        {
            SelectedDateFrom = selectedDateFrom;
            SelectedDateTo = selectedDateTo;
        }
    }
}
