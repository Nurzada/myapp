﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTransfer.ViewModels
{
    public class AddBalanceViewModel
    {
        [Required]
        [Display(Name = "Счет")]
        public string PersonalAccount { get; set; }

        [Required]
        [Display(Name = "Сумма")]
        public int Sum { get; set; }
    }
}
