﻿using Microsoft.AspNetCore.Mvc;
using MoneyTransfer.Controllers;
using MoneyTransfer.Models;
using System;
using Xunit;

namespace MoneyTransfer.Tests
{
    public class AccountControllerTests
    {
        private Random rand;
        private Checks checks;
       //private ApplicationContext context;

        public AccountControllerTests()
        {
            rand = new Random();
            checks = new Checks();
            //this.context = context;
        }

        [Fact]
        public void CheckLengthGeneratedPersonalAccount()
        {
            int result = checks.PersonalAccountGeneration(rand).Length;
            Assert.Equal(6, result);
        }

        //[Fact]
        //public void CheckUserToExistence()
        //{
        //    User user = new User();
        //    bool result = checks.CheckForUserExistence("533050", ref user, context);
        //    Assert.Equal(false, result);
        //}

        //[Fact]
        //public void CheckUniqueness()
        //{
        //    bool result = checks.CheckForUniqueness("533050", context);
        //    Assert.Equal(true, result);
        //}

        [Fact]
        public void CheckSum()
        {
            User user = new User()
            {
                Email = "Nurzada",
                Balance = 3000,
                PersonalAccount = "123456"
            };
            bool result = checks.CheckForRequiredSum(2000, user);
            Assert.Equal(true, result);
        }
    }
}
